#include <string>
#include <iostream>
#include <cassert>
#include <vector>
#include <map>

using namespace std;

enum class QueryType {
  NewBus,
  BusesForStop,
  StopsForBus,
  AllBuses
};

struct Query {
  QueryType type;
  string bus;
  string stop;
  vector<string> stops;
};

istream& operator >> (istream& is, Query& q) {
  // Реализуйте эту функцию
  cout << "Start" << endl;
  string operation_code;
  is >> operation_code;
  if (operation_code=="NEW_BUS"){
      int count;
      string bus_stop;
      q.type=QueryType::NewBus;
      is >> q.bus;
      is >> count;
      for (auto i = 0; i < count; i++)
      {
          is >> bus_stop;
          q.stops.push_back(bus_stop);
      }
      
  }
  if (operation_code=="BUSES_FOR_STOP"){
      q.type=QueryType::BusesForStop;
      is >> q.stop;
  }
  if (operation_code=="STOPS_FOR_BUS"){
      q.type=QueryType::StopsForBus;
  }
  if (operation_code=="ALL_BUSES"){
      q.type=QueryType::AllBuses;
  }
  return is;
}

int main(){


int query_count;
  Query q;

  cin >> query_count;

  for (int i = 0; i < query_count; ++i) {
    cin >> q;
  }

    return 0;
}


