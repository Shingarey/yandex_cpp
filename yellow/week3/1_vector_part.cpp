#include<vector>
#include<iostream>
#include<algorithm>

using namespace std;

void PrintVectorPart(const vector<int> & vec){
    auto negative_it = find_if(
        vec.begin(),
        vec.end(),
        [](const int & number){
            return number < 0;
        }
    );

    while (negative_it != vec.begin())
    {
        cout << *(--negative_it) << " ";
    }
    
}

// void PrintReverse(vector<int>::iterator begin, vector<int>::iterator end){
//     while(end != begin){
//         --end;
//         cout << *end << " ";
//     }
// }
// 
// void PrintVectorPart(vector<int> vec){
//     bool foung_negative = false;
//     for (auto it=vec.begin(); it !=vec.end();it++)
//     {
//         if (*it < 0){
//             PrintReverse(vec.begin(), it);
//             foung_negative=true;
//             it=vec.end()-1;
//         }
//     }
//     if (foung_negative==false){
//         PrintReverse(vec.begin(), vec.end());
//         }
// }

int main() {
  PrintVectorPart({6, 1, 8, -5, 4});
  cout << endl;
  PrintVectorPart({-6, 1, 8, -5, 4});  // ничего не выведется
  cout << endl;
  PrintVectorPart({6, 1, 8, 5, 4});
  cout << endl;
  return 0;
}