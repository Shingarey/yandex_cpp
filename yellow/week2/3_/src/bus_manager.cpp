#pragma once

#include "responses.h"
#include "query.h"
#include "bus_manager.h"


  void BusManager::AddBus(const string& bus, const vector<string>& stops) {
    // Реализуйте этот метод
    //buses_to_stops[bus] = stops;
    buses_to_stops[bus].insert(buses_to_stops[bus].end(), stops.begin(), stops.end());
    for (auto stop : stops)
    {
      stops_to_buses[stop].push_back(bus);
    }
    
  }

  BusesForStopResponse BusManager::GetBusesForStop(const string& stop) const {
    // Реализуйте этот метод
    if (stops_to_buses.count(stop) > 0){
      return {stop, stops_to_buses.at(stop)};
    }
    else
    {
      return {"",{}};
    }
    
  }

  StopsForBusResponse BusManager::GetStopsForBus(const string& bus) const {
    // Реализуйте этот метод
    if (buses_to_stops.count(bus) > 0){
      return {bus, buses_to_stops.at(bus), stops_to_buses}   ;   
    }
    else
    {
      return {"",{},{}};
    }
    
  }

  AllBusesResponse BusManager::GetAllBuses() const {
    // Реализуйте этот метод
    return {buses_to_stops};
  }