#pragma once

#include "responses.h"
#include "query.h"

ostream& operator << (ostream& os, const BusesForStopResponse& r) {
  // Реализуйте эту функцию
  if (r.stop.size() != 0)
  {
    os << "Stop " << r.stop << ": ";
    for (auto bus : r.buses)
    {
      os << bus << " ";
    }
  }
  else
  {
    os << "No stop";
  }
  os << endl;
  
  return os;
}

ostream& operator << (ostream& os, const StopsForBusResponse& r) {
  // Реализуйте эту функцию
  if (r.bus.size() != 0)
  {
    //os << "Bus " << r.bus << ": ";
    for (auto stop : r.stops)
    {
      os << "Stop " << stop << ": ";
      if (r.buses.at(stop).size() == 1 and r.buses.at(stop).front() == r.bus)
      {
        os << "no interchange" << endl;
      }
      else
      {
        for (auto bus : r.buses.at(stop))
        {
          if (bus != r.bus){
            os << bus << " ";
          }
        }
        os << endl;
      }
    }
  }
  else
  {
    os << "No bus" << endl;
  }
  
  
  return os;
}


ostream& operator << (ostream& os, const AllBusesResponse& r) {
  // Реализуйте эту функцию
  if (r.stops_to_buses.size() != 0)
  {
    for (auto item : r.stops_to_buses)
    {
      os << "Bus " << item.first << ": ";
      for (auto stop : item.second)
      {
          os << stop << " ";
      }
      os << endl;
    }
  }
  else
  {
    os << "No buses";
  }
    
  return os;
}
