#pragma once

#include "query.h"

istream& operator >> (istream& is, Query& q) {
  // Реализуйте эту функцию
  string new_stop;
  string command;
  int N;
  is >> command;
  is.ignore(1);
  if (command == "NEW_BUS")
  {
    q.type = QueryType::NewBus;
    is >> q.bus;
    is.ignore(1);
    is >> N;
    q.stops = {};
    for (size_t i = 0; i < N; i++)
    {
      is.ignore(1);
      is >> new_stop;
      q.stops.push_back(new_stop);
    }
  }
  if (command == "BUSES_FOR_STOP")
  {
    q.type = QueryType::BusesForStop;
    is >> q.stop;
  }
  if (command == "STOPS_FOR_BUS")
  {
    q.type = QueryType::StopsForBus;
    is >> q.bus;
  }
  if (command == "ALL_BUSES")
  {
    q.type = QueryType::AllBuses;
  }
  
  return is;
}