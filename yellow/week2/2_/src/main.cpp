#include "phone_number.h"

#include <iostream>

int main(int argc, char *argv[])
{
	PhoneNumber Test("+123-234-123");
	std::cout << Test.GetCountryCode() << std::endl;
	std::cout << Test.GetCityCode() << std::endl;
	std::cout << Test.GetLocalNumber() << std::endl;
	std::cout << Test.GetInternationalNumber() << std::endl;

}