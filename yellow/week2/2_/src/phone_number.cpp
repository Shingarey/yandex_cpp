#include "phone_number.h"

#include<sstream>
#include<iostream>


PhoneNumber::PhoneNumber(const string &international_number){
    std::stringstream stream(international_number);
    try
    {
        if (international_number.size() == 0){
            throw runtime_error("Empty number");
        }
        if (stream.peek() != '+'){
            throw runtime_error("Invalid_argument");
            }
        stream.ignore(1);
        std::getline(stream, this->country_code_,'-');
        std::getline(stream, this->city_code_,'-');
        std::getline(stream, this->local_number_,'-');
        if (this->country_code_.size()==0 || this->city_code_.size() ==0 ||this->local_number_.size() ==0){
            throw runtime_error("Invalid_argument");
        }
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
}


    string PhoneNumber::GetCountryCode() const{
        return this->country_code_;
    }
    string PhoneNumber::GetCityCode() const{
        return this->city_code_;
    }
    string PhoneNumber::GetLocalNumber() const{
        return this->local_number_;
    }
    string PhoneNumber::GetInternationalNumber() const{
        return "+" + this->country_code_ + "-" + this->city_code_ + "-" + this->local_number_;
    }