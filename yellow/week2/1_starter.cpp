#include <string>
#include <iostream>
#include <cassert>
#include <vector>
#include <map>

using namespace std;

enum class QueryType {
  NewBus,
  BusesForStop,
  StopsForBus,
  AllBuses
};

struct Query {
  QueryType type;
  string bus;
  string stop;
  vector<string> stops;
};

istream& operator >> (istream& is, Query& q) {
  // Реализуйте эту функцию
  string new_stop;
  string command;
  int N;
  is >> command;
  is.ignore(1);
  if (command == "NEW_BUS")
  {
    q.type = QueryType::NewBus;
    is >> q.bus;
    is.ignore(1);
    is >> N;
    q.stops = {};
    for (size_t i = 0; i < N; i++)
    {
      is.ignore(1);
      is >> new_stop;
      q.stops.push_back(new_stop);
    }
  }
  if (command == "BUSES_FOR_STOP")
  {
    q.type = QueryType::BusesForStop;
    is >> q.stop;
  }
  if (command == "STOPS_FOR_BUS")
  {
    q.type = QueryType::StopsForBus;
    is >> q.bus;
  }
  if (command == "ALL_BUSES")
  {
    q.type = QueryType::AllBuses;
  }
  
  return is;
}

struct BusesForStopResponse {
  // Наполните полями эту структуру
  string stop;
  vector<string> buses;
};

ostream& operator << (ostream& os, const BusesForStopResponse& r) {
  // Реализуйте эту функцию
  if (r.stop.size() != 0)
  {
    os << "Stop " << r.stop << ": ";
    for (auto bus : r.buses)
    {
      os << bus << " ";
    }
  }
  else
  {
    os << "No stop";
  }
  os << endl;
  
  return os;
}

struct StopsForBusResponse {
  // Наполните полями эту структуру
  string bus;
  vector<string> stops;
  map<string, vector<string>> buses;
};

ostream& operator << (ostream& os, const StopsForBusResponse& r) {
  // Реализуйте эту функцию
  if (r.bus.size() != 0)
  {
    //os << "Bus " << r.bus << ": ";
    for (auto stop : r.stops)
    {
      os << "Stop " << stop << ": ";
      if (r.buses.at(stop).size() == 1 and r.buses.at(stop).front() == r.bus)
      {
        os << "no interchange" << endl;
      }
      else
      {
        for (auto bus : r.buses.at(stop))
        {
          if (bus != r.bus){
            os << bus << " ";
          }
        }
        os << endl;
      }
    }
  }
  else
  {
    os << "No bus" << endl;
  }
  
  
  return os;
}

struct AllBusesResponse {
  // Наполните полями эту структуру
  map<string, vector<string>> stops_to_buses;
};

ostream& operator << (ostream& os, const AllBusesResponse& r) {
  // Реализуйте эту функцию
  if (r.stops_to_buses.size() != 0)
  {
    for (auto item : r.stops_to_buses)
    {
      os << "Bus " << item.first << ": ";
      for (auto stop : item.second)
      {
          os << stop << " ";
      }
      os << endl;
    }
  }
  else
  {
    os << "No buses";
  }
    
  return os;
}

class BusManager {
public:
  void AddBus(const string& bus, const vector<string>& stops) {
    // Реализуйте этот метод
    //buses_to_stops[bus] = stops;
    buses_to_stops[bus].insert(buses_to_stops[bus].end(), stops.begin(), stops.end());
    for (auto stop : stops)
    {
      stops_to_buses[stop].push_back(bus);
    }
    
  }

  BusesForStopResponse GetBusesForStop(const string& stop) const {
    // Реализуйте этот метод
    if (stops_to_buses.count(stop) > 0){
      return {stop, stops_to_buses.at(stop)};
    }
    else
    {
      return {"",{}};
    }
    
  }

  StopsForBusResponse GetStopsForBus(const string& bus) const {
    // Реализуйте этот метод
    if (buses_to_stops.count(bus) > 0){
      return {bus, buses_to_stops.at(bus), stops_to_buses}   ;   
    }
    else
    {
      return {"",{},{}};
    }
    
  }

  AllBusesResponse GetAllBuses() const {
    // Реализуйте этот метод
    return {buses_to_stops};
  }
  private:
     map<string, vector<string>> buses_to_stops, stops_to_buses;
};

// Не меняя тела функции main, реализуйте функции и классы выше

int main() {
  int query_count;
  Query q;

  cin >> query_count;

  BusManager bm;
  for (int i = 0; i < query_count; ++i) {
    cin >> q;
    switch (q.type) {
    case QueryType::NewBus:
      bm.AddBus(q.bus, q.stops);
      break;
    case QueryType::BusesForStop:
      cout << bm.GetBusesForStop(q.stop) << endl;
      break;
    case QueryType::StopsForBus:
      cout << bm.GetStopsForBus(q.bus) << endl;
      break;
    case QueryType::AllBuses:
      cout << bm.GetAllBuses() << endl;
      break;
    }
  }

  return 0;
}
