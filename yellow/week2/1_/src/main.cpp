#include "sum_reverse_sort.h"

#include <iostream>

int main(int argc, char *argv[])
{
	std::cout << Test::Sum(2, 3) << std::endl;
	std::cout << Reverse("Test") << std::endl;
	std::vector<int> vec = {3,2,1};
	Sort(vec);

	for (auto i : vec){
		std::cout << i << " ";
	}
	std::cout << endl;
}