#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>
#include <random>

using namespace std;

template <class T>
ostream& operator << (ostream& os, const vector<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class T>
ostream& operator << (ostream& os, const set<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class K, class V>
ostream& operator << (ostream& os, const map<K, V>& m) {
  os << "{";
  bool first = true;
  for (const auto& kv : m) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class T, class U>
void AssertEqual(const T& t, const U& u, const string& hint = {}) {
  if (t != u) {
    ostringstream os;
    os << "Assertion failed: " << t << " != " << u;
    if (!hint.empty()) {
       os << " hint: " << hint;
    }
    throw runtime_error(os.str());
  }
}

void Assert(bool b, const string& hint) {
  AssertEqual(b, true, hint);
}

class TestRunner {
public:
  template <class TestFunc>
  void RunTest(TestFunc func, const string& test_name) {
    try {
      func();
      cerr << test_name << " OK" << endl;
    } catch (exception& e) {
      ++fail_count;
      cerr << test_name << " fail: " << e.what() << endl;
    } catch (...) {
      ++fail_count;
      cerr << "Unknown exception caught" << endl;
    }
  }

  ~TestRunner() {
    if (fail_count > 0) {
      cerr << fail_count << " unit tests failed. Terminate" << endl;
      exit(1);
    }
  }

private:
  int fail_count = 0;
};

int SolveQuadtraticEquation(const double & a,const double & b,const double & c) {
  if (b == 0 && c != 0){
      return 1;
  }
  else if ((b == 0 && c == 0)){
    return 0;
  }
  else
  {
    double x1 = (-b + sqrt(b*b -4*a*c))/(2*a);
    double x2 = (-b - sqrt(b*b -4*a*c))/(2*a);
    //cout << x1 << ", " << x2 << endl;
    if (abs(x1) != abs(x2) ){
      return 2;
    }
    else
    {
      return 1;
    }
  }
}

int GetDistinctRealRootCount(double a, double b, double c) {
  // Вы можете вставлять сюда различные реализации функции,
  // чтобы проверить, что ваши тесты пропускают корректный код
  // и ловят некорректный
  if (a != 0)
  {
    return SolveQuadtraticEquation(a,b,c);
  }
  else
  {
    return 1;
  }
}

void TestTwoRoots (){
	mt19937 gen;
	uniform_real_distribution<> unif(-10, 10);
  for (auto i = 0; i < 100; ++i) {
		const auto a = unif(gen);
		const auto b = unif(gen);
		const auto c = unif(gen);

		const auto count = GetDistinctRealRootCount(a, b, c);

		Assert(count >= 0 && count <= 2, "Quadratic equation has only 0, 1 or 2 real roots.");
}
}

void TestOneRoot (){
	mt19937 gen;
	uniform_real_distribution<> unif(-10, 10);

 for (auto i = 0; i < 100; ++i) {
		const auto a = 1;
		const auto x1 = unif(gen);
		
    const auto b = x1 + x1;
    const auto c = x1 * x1;

		const auto count = GetDistinctRealRootCount(a, b, c);

		AssertEqual(count, 1, "Quadratic equation has only 1 real root.");
 }
}

 void TestLinearEquation (){
	mt19937 gen;
	uniform_real_distribution<> unif(-10, 10);

 for (auto i = 0; i < 100; ++i) {
		const auto b = unif(gen);
		const auto c = unif(gen);
		
    const auto count = GetDistinctRealRootCount(0.0, b, c);

		AssertEqual(count, 1, "Linear equation has only 1 real root.");
 }
}

 void TestConst (){
	mt19937 gen;
	uniform_real_distribution<> unif(-10, 10);

 for (auto i = 0; i < 100; ++i) {
    const auto a = unif(gen);
		
    const auto count = GetDistinctRealRootCount(a, 0.0, 0.0);

		AssertEqual(count, 0, "Test Const has no roots.");
 }
}

int main() {
  TestRunner runner;
  runner.RunTest(TestTwoRoots, "TestTwoRoots");
  runner.RunTest(TestOneRoot, "TestOneRoot");
  runner.RunTest(TestLinearEquation, "TestLinearEquation");
  runner.RunTest(TestConst, "TestConst");
  // добавьте сюда свои тесты
  return 0;
}
