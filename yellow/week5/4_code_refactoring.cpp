#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Person{
public:
    Person(const string & name, const string & type):Name(name), Type(type){};

    const string Name;
    const string Type;

    virtual void Walk(const string & destination) {
        cout << Type << ": " << Name << " walks to: " << destination << endl;
    };
};

class Student : public Person {
public:

    Student(const string & name, const string & favouriteSong) : Person(name, "Student") {
        FavouriteSong = favouriteSong;
    }

    void Learn() {
        cout << Type << ": " << Name << " learns" << endl;
    }

    void Walk(const string & destination) override {
        cout << Type << ": " << Name << " walks to: " << destination << endl;
        SingSong();
    }

    void SingSong() {
        cout << Type << ": " << Name << " sings a song: " << FavouriteSong << endl;
    }

public:
    string FavouriteSong;
};


class Teacher : public Person{
public:

    Teacher(const string & name, const string & subject) : Person(name, "Teacher"){
        Subject = subject;
    }

    void Teach() {
        cout << Type << ": " << Name << " teaches: " << Subject << endl;
    }

public:
    string Subject;
};


class Policeman : public Person{
public:
    Policeman(const string & name) : Person(name, "Policeman") {
    }

    template <typename T >
    void Check(T t) {
        cout << Type << ": " << Name << " checks " << t.Type << ". "<< t.Type <<"'s name is: " << t.Name << endl;
    }

public:
    string Name;
};

template <typename T >
void VisitPlaces(T & t, const vector<string> & places) {
    for (auto p : places) {
        t.Walk(p);
    }
}

int main() {
    Teacher t("Jim", "Math");
    Student s("Ann", "We will rock you");
    Policeman p("Bob");

    VisitPlaces(t, {"Moscow", "London"});
    p.Check(s);
    VisitPlaces(s, {"Moscow", "London"});
    return 0;
}
