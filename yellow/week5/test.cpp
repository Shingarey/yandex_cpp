#include <iostream>
#include <string>

using namespace std;

int main(){
    int  * pInt = new int(20);
    string  * s = new string("Test");
    cout << s->size() << endl;
    cout << pInt << endl;
    cout << * pInt << endl;

    int & ref_to_int = * pInt;

    int to_int = * pInt;

    cout << ref_to_int << endl;

    cout << to_int << endl;

    ref_to_int = 30;

    to_int = 40;

    cout << * pInt << endl;

    cout << to_int << endl;

    cout << * pInt << endl;

    return 0;
}