#include <iostream>

using namespace std;

class Animal {
public:
    Animal(const string & n, const string & c) 
    : Name(n)
    , Color(c){
    };

    const string Name;
    const string Color;
};


class Dog : public Animal {
public:
    Dog () : Animal("Dog", "black"){};

    void Bark() {
        cout << Color << " " << Name << " barks: woof!" << endl;
    }
};

int main(){
    Dog d;
    d.Bark();
}