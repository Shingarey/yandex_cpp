#include <iostream>
#include <string>
#include <memory>
#include <vector>
#include <sstream>
#include <iomanip>

using namespace std;

const double PI = 3.14;

class Figure {
    public:
    Figure(const string & name, const vector<int> & params) : name_(name), params_(params){};

    const string name_;
    const vector<int> params_;

    virtual string Name() = 0;
    virtual double Perimeter() = 0;
    virtual double Area() = 0;
};

class Triangle : public Figure
{
public:
    Triangle(const string & name1, const vector<int> & params1) : Figure(name1, params1){};
    
    string Name() override {
        return name_;
    }
    double Perimeter() override {
        return (params_[0]+params_[1]+params_[2]);
    }
    double Area() override {
        return (params_[0]+params_[1]+params_[2])/2.0;
    }
};

class Rect : public Figure
{
public:
    Rect(const string & name1, const vector<int> & params1) : Figure(name1, params1){};
    
    string Name() override {
        return name_;
    }
    double Perimeter() override {
        return 2.0*(params_[0]+params_[1]);
    }
    double Area() override {
        return (params_[0]*params_[1]);
    }
};

class Circle : public Figure
{
public:
    Circle(const string & name1, const vector<int> & params1) : Figure(name1, params1){};
    
    string Name() override {
        return name_;
    }
    double Perimeter() override {
        return (2.0*PI*params_[0]);
    }
    double Area() override {
        return (PI*params_[0]*params_[0]);
    }
};

shared_ptr<Figure> CreateFigure(istringstream & is){
    string name;
    vector<int> params;

    getline(is, name, ' ');

    for (string param; getline(is, param, ' ');)
    {
        params.push_back(stoi(param));
    }
    
    if (name == "RECT")
    {
         return make_shared<Rect>(name, params);
    }
    if (name == "TRIANGLE")
    {
        return make_shared<Triangle>(name, params);
    }
    if (name == "CIRCLE")
    {
         return make_shared<Circle>(name, params);
    }
    
    

    return 0;
}

int main() {
  vector<shared_ptr<Figure>> figures;
  for (string line; getline(cin, line); ) {
    istringstream is(line);

    string command;
    is >> command;
    if (command == "ADD") {
      // Пропускаем "лишние" ведущие пробелы.
      // Подробнее об std::ws можно узнать здесь:
      // https://en.cppreference.com/w/cpp/io/manip/ws
      is >> ws;
      figures.push_back(CreateFigure(is));
    } else if (command == "PRINT") {
      for (const auto& current_figure : figures) {
        cout << fixed << setprecision(3)
             << current_figure->Name() << " "
             << current_figure->Perimeter() << " "
             << current_figure->Area() << endl;
      }
    }
  }
  return 0;
}