#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

template <typename T>
ostream & operator<< (ostream& os, const vector<T>& v){
    for (auto it = v.rbegin(); it != v.rend(); ++it)
    {
        os << *it << " ";
    }
    os << endl;
    return os;
    
}

vector<int> CreateVector (int & N){
    vector<int> v;
    for (size_t i = 1; i <= N; i++)
    {
        v.push_back(i);   
    }
    return v;
}

template <typename T>
void FindCombinations (vector<T> v){

    sort(v.begin(), v.end());
    do {
        std::cout << v << '\n';
    } while(next_permutation(v.begin(), v.end()));
}

int main(){
    int N = 3;  
    FindCombinations(CreateVector(N));
    vector<string> S = {"d", "b", "c"};
    FindCombinations(S);
}