#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

void PrintVectorPart(const vector<int>& numbers){
    auto neg_it = find_if(begin(numbers),
            end(numbers),
            [](const int number){return number < 0;} );
    while (neg_it != begin(numbers))
        {
            --neg_it;
            cout << *neg_it << " "; 
        }
}

int main() {
  PrintVectorPart({6, 1, 8, -5, 4});
  cout << endl;
  PrintVectorPart({-6, 1, 8, -5, 4});  // ничего не выведется
  cout << endl;
  PrintVectorPart({6, 1, 8, 5, 4});
  cout << endl;
  return 0;
}