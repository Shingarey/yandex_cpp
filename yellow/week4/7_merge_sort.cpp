#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
#include <set>

using namespace std;

template <typename RandomIt>
void MergeSort2(RandomIt range_begin, RandomIt range_end){

    if (range_begin+1 != range_end){
        vector<typename RandomIt::value_type> elements(range_begin, range_end);
        vector<typename RandomIt::value_type> tmp_array(range_end-range_begin);

        auto middle = (range_end - range_begin)/2;
        vector<typename RandomIt::value_type> left(range_begin, range_begin + middle);
        vector<typename RandomIt::value_type> right(range_begin + middle, range_end);

        MergeSort2(left.begin(), left.end());
        MergeSort2(right.begin(), right.end());

        auto l_iter = left.begin();
        auto l_end = left.end();
        auto r_iter = right.begin();
        auto r_end = right.end();

        auto tmp_iter = tmp_array.begin();


        while (l_iter != l_end || r_iter != r_end)
        {
            if (*l_iter < *r_iter)
            {
                *tmp_iter = move(*l_iter);
                ++l_iter;
                ++tmp_iter;
            }
            else
            {
                *tmp_iter = move(*r_iter);
                ++r_iter;
                ++tmp_iter;
            }
            if (l_iter == l_end)
            {
                copy(
                            make_move_iterator(r_iter),
                            make_move_iterator(r_end),
                            tmp_iter
                );
                break;
            }
            if (r_iter == r_end)
            {
                copy(
                            make_move_iterator(l_iter),
                            make_move_iterator(l_end),
                            tmp_iter
                );
                    break;
                }
            }
        copy(tmp_array.begin(), tmp_array.end(), range_begin);
    }
} 
//
//void MergeSort3(RandomIt range_begin, RandomIt range_end){
//    vector<typename RandomIt::value_type> elements(range_begin, range_end);
//    vector<typename RandomIt::value_type> sorted;
//    
//    auto one_third = (range_end - range_begin)/3;
//    vector<typename RandomIt::value_type> first(range_begin, range_begin + one_third);
//    vector<typename RandomIt::value_type> second(range_begin + one_third, range_begin + one_third*2);
//    vector<typename RandomIt::value_type> third(range_begin + one_third*2, range_end);
//    
//    sort(first.begin(), first.end());
//    sort(second.begin(), second.end());
//    sort(third.begin(), third.end());
//    
//    merge(first.begin(), first.end(), 
//          second.begin(), second.end(),
//          back_inserter(sorted));
//    
//    copy(sorted.begin(), sorted.end(), range_begin);
//} 
//
int main() {
  vector<int> v = {6, 4, 7, 6, 4, 4, 2 , 9, 0, 1};
  MergeSort2(begin(v), end(v));
  for (int x : v) {
    cout << x << " ";
  }
  cout << endl;
  vector<string> s = {"a", "f","g","e","k","s"};
  MergeSort2(begin(s), end(s));
  for (auto x : s) {
    cout << x << " ";
  }

  return 0;
}
