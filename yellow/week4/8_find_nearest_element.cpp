#include <iostream>
#include <set>
#include <algorithm>

using namespace std;

// set<int>::const_iterator —
// тип итераторов для константного множества целых чисел

set<int>::const_iterator FindNearestElement(
    const set<int>& numbers,
    int border)
    {
        auto it = lower_bound(numbers.begin(), numbers.end(), border);

        if (it == numbers.begin()) {
            return it;
        }
        const auto last_less = prev(it);
        if (it == numbers.end()) {
          return last_less;
        }
        const bool is_left = (border - *last_less <= *it - border);
        return is_left ? prev(it) : it;
    }


int main() {
  set<int> numbers = {1, 4, 6};
  cout <<
      *FindNearestElement(numbers, 0) << " " <<
      *FindNearestElement(numbers, 3) << " " <<
      *FindNearestElement(numbers, 5) << " " <<
      *FindNearestElement(numbers, 6) << " " <<
      *FindNearestElement(numbers, 100) << endl;
      
  set<int> empty_set;
  
  cout << (FindNearestElement(empty_set, 8) == end(empty_set)) << endl;
  return 0;
}