#include <string>
#include <vector>
#include <queue>
#include <map>
#include <algorithm>
#include <iostream>

using namespace std;

string FindNameByYear (const map<int, string> & names, int year ) {
  auto iter_after = names.upper_bound(year);
  string name;
  if (iter_after != names.begin())
  {
    name = prev(iter_after)->second;
  }
  return name;
}

class Person {
public:
  void ChangeFirstName(int year, const string& first_name) {
    // добавить факт изменения имени на first_name в год year
    first_names[year] = first_name;
  }
  void ChangeLastName(int year, const string& last_name) {
    // добавить факт изменения фамилии на last_name в год year
    last_names[year] = last_name;
    }

  string GetFullName(int year) {
    // получить имя и фамилию по состоянию на конец года year
    // с помощью двоичного поиска
    const string first_name = FindNameByYear(first_names, year);
    const string last_name = FindNameByYear(last_names, year);
    if ((first_name.empty()) && (last_name.empty())) {
      return "Incognito";
    }
    else if (last_name.empty()){
      return first_name + " with unknown first name";
    }
    else if (first_name.empty()){
      return last_name + " with unknown last name";
    }

    return first_name + " " + last_name;
    

  }
  
  map<int, string> first_names;
  map<int, string> last_names;
};

int main() {
  Person person;
  
  person.ChangeFirstName(1965, "Polina");
  person.ChangeLastName(1967, "Sergeeva");
  for (int year : {1900, 1965, 1990}) {
    cout << person.GetFullName(year) << endl;
  }
  
  person.ChangeFirstName(1970, "Appolinaria");
  for (int year : {1969, 1970}) {
    cout << person.GetFullName(year) << endl;
  }
  
  person.ChangeLastName(1968, "Volkova");
  for (int year : {1969, 1970}) {
    cout << person.GetFullName(year) << endl;
  }
  
  return 0;
}