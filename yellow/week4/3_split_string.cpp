#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;


vector<string> SplitIntoWords(const string& s){
    auto it = s.begin();
    auto begin = s.begin();
    vector<string> vec;

    while (it != s.end())
    {
        it = find(begin, s.end(), ' '); 
        string s2(begin, it);  
        vec.push_back(s2);
        begin = it+1;
    }
    return vec;
    
}

int main() {
  string s = "C2 Cpp Java Python";

  vector<string> words = SplitIntoWords(s);
  cout << words.size() << " ";
  for (auto it = begin(words); it != end(words); ++it) {
    if (it != begin(words)) {
      cout << "/";
    }
    cout << *it;
  }
  cout << endl;
  
  return 0;
}