#include <iostream>
#include <vector>
#include <string>
#include <map>
using namespace std;

struct Person
{
    string name;
    string surname;
    int age;
};


int main()
{
    vector<Person> staff;
    staff.push_back({"Ivan","Ivanov",25});
    std::cout << staff[0].surname << endl;
    map<string,int> name_to_value;
    name_to_value["one"] = 1;
    std::cout << "One is: " << name_to_value["one"] << endl;
    cout << 1/0.4 << endl;
    return 0;
}
